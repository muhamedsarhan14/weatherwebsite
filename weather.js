window.addEventListener("load",function(){
 let long;
 let lat;
 let tempDegree = document.querySelector(".tempaurature-degree");
 let tempDes = document.querySelector(".tempaurature-describtion");
 let locZone = document.querySelector(".location-zone");
 let secDegree = document.querySelector(".section-degree");
 let secSpan = document.querySelector(".section-degree span");

if (navigator.geolocation) {
    navigator.geolocation.getCurrentPosition(position => {

       long = position.coords.longitude;
       lat  = position.coords.latitude;

       const proxy = "https://cors-anywhere.herokuapp.com/";
       const api = `${proxy}https://api.darksky.net/forecast/16da8d3a5fb817c1f4c83f0b51d6424b/${lat},${long}`;
    
       fetch(api)
       .then(response => {
        return response.json();
    })
       .then(data => {
           console.log(data);
        const  {temperature , summary , icon }   = data.currently;
            
       tempDegree.textContent = temperature;
       tempDes.textContent = summary;
       locZone.textContent = data.timezone ;
       let celicius = (temperature - 32)* ( 5 / 9 ) ;
       setIcones(icon , this.document.querySelector(".icone"));

       secDegree.addEventListener("click" ,()=>{
           if(secSpan.textContent === "F"){
               secSpan.textContent = "C";
               tempDegree.textContent = Math.floor(celicius);
           }else{
            secSpan.textContent = "F";
            tempDegree.textContent = temperature;
           }

       })

       })
    })
    
}else
{
    this.alert("Please allow us to access your Loacation");
}
function setIcones(icon , iconID){
    const skycons = new Skycons({color: "white"});
    const currentIcone = icon.replace(/-/g,"_").toUpperCase();
    skycons.play();
    return skycons.set(iconID, Skycons[currentIcone]);
}

})